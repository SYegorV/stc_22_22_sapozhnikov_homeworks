package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        CashMachine cashMachine = new CashMachine(50000, 60000, 100000);

        while (true) {
            System.out.println(" Нажмите - 1 для снятия \n Нажмите - 2 для пополнения \n Нажмите - 0 для завершения ");
            System.out.print(" Ввод действия: ");

            Scanner scanner = new Scanner(System.in);
            int number = scanner.nextInt();

            if (number == 1) {
                System.out.println(" Максимальная сумма, разрешенная к выдаче 60 000");
                System.out.print(" Введите сумму для снятия: ");
                cashMachine.outMoney(scanner.nextInt());
                continue;
            }

            if (number == 2) {
                System.out.print(" Введите сумму для зачисления денежных средств на счет: ");
                int numberOut; // сумма, которая не была положена на счет
                numberOut = cashMachine.inMoney(scanner.nextInt());
                if (numberOut == 0) {

                } else {
                    System.out.println(" сумма, которая не была положена на счет: " + numberOut);
                }
                continue;
            }

            if (number == 0) {
                System.out.println(" хорошего дня");
                break;
            }

            if (number == 3) {
                System.out.print(" Количество проведенных операций: ");
                cashMachine.operation();
                continue;
            }
        }
    }
}
