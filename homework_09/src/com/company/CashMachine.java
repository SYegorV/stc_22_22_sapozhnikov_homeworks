package com.company;

public class CashMachine {
    static int moneyInCashMachine; // Сумма оставшихся денег в банкомате
    static int maxSumOut; // Максимальная сумма, разрешенная к выдаче
    static int maxSumInCashMachine; // Максимальный сумма денег (сумма, которая может быть в банкомате)
    static int increment = 0; // Количество проведенных операций

    public static int outMoney(int getMoney) { // Выдать деньги // (не больше, чем разрешено и осталось в банкомате, метод возвращает сумму, которая была в итоге выдана)
        if (getMoney <= maxSumOut && getMoney <= moneyInCashMachine) {
            System.out.println(" выданные money: " + getMoney);
            increment++;
            return getMoney;
        } else if (getMoney <= maxSumOut && getMoney >= moneyInCashMachine) {
            System.out.println(" выданные money: " + moneyInCashMachine);
            increment++;
            return moneyInCashMachine;
        } else {
            increment++;
            return 0;
        }
    }

    public static int inMoney(int putOn) { // Положить деньги // если превышен объем, метод должен вернуть сумму, которая не была положена на счет
        if (putOn + moneyInCashMachine > maxSumInCashMachine) {
            System.out.println(" пополнили денежные средства на счет, на: " + (putOn - (putOn + moneyInCashMachine - maxSumInCashMachine)));
            increment++;
            return putOn + moneyInCashMachine - maxSumInCashMachine;
        } else {
            System.out.println(" пополнили денежные средства на счет, на: " + putOn);
            increment++;
            return 0;
        }
    }

    public static void operation() {
        System.out.println(increment);
    }

    public CashMachine(int moneyInCashMachine, int maxSumOut, int maxSumInCashMachine) { // Constructor
        this.moneyInCashMachine = moneyInCashMachine;
        this.maxSumOut = maxSumOut;
        this.maxSumInCashMachine = maxSumInCashMachine;
    }
}
