package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input a lenght of array: ");
        int arrayCapacity = in.nextInt();
        int[] arr = new int[arrayCapacity];

        for (int i =0; i < arr.length; i++) {
            arr[i] = in.nextInt();
        }

        int i = 0, right = 0, left = 0, mid = 0, localMin = 0;

        while (i != arr.length) {
            left = mid;
            mid = right;
            right = arr[i++];
            if (right > mid && mid < left) {
                System.out.println("local min: " + localMin);
                localMin++;
            }
        }

        if (i >= 1 && right < mid) {
            System.out.println("local min: " + right);
            localMin++;
        }

        System.out.println(Arrays.toString(arr));
        System.out.println("Count of local min: " + localMin);
    }
}
