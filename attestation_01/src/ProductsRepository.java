import java.util.List;

public interface ProductsRepository {
    List<Product> findProductById (Integer id);
    List<Product> findAll();
}
