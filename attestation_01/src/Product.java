public class Product {
    private Integer id;
    private String nameProduct;
    private Double cost;
    private Integer inStock;

    public Product(Integer id, String nameProduct, Double cost, Integer inStock) {
        this.id = id;
        this.nameProduct = nameProduct;
        this.cost = cost;
        this.inStock = inStock;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public Integer getInStock() {
        return inStock;
    }

    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }
}
