import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        ProductsRepository productsRepository = new ProductsRepositoryFileBasedImpl("FileList.txt");
        System.out.println(productsRepository.findAll());
        System.out.println(productsRepository.findProductById(1));
    }
}
