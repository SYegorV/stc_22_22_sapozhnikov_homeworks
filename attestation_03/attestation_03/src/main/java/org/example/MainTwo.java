package org.example;

import org.example.services.UsersServiceImpl;
import org.example.validator.EmailSimpleValidator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainTwo {
    public static void main(String[] args) {
        // = его реализация:
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml"); // будет искать файл - context.xml

//        // получение объектов:
//        EmailSimpleValidator emailSimpleValidator = (EmailSimpleValidator) context.getBean("emailSimpleValidator"); // по названию: id="emailSimpleValidator" // Object - явное преобразование
//        EmailSimpleValidator emailSimpleValidator1 = context.getBean(EmailSimpleValidator.class); // указать тип - Two version
//                                                                                                // можно интерфейс указывать
//        emailSimpleValidator.validate("06qwerty001@gmail.com");

        UsersServiceImpl usersService = context.getBean(UsersServiceImpl.class);
        usersService.singUp("11aqua011@gmail.com", "123456aqua11");


    }
}
