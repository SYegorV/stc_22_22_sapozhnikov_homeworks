package org.example.services;

import lombok.RequiredArgsConstructor; // замена конструктора
import org.example.model.User;
import org.example.repository.UsersRepository;
import org.example.validator.EmailSimpleValidator;
import org.example.validator.PasswordLengthValidator;

// @RequiredArgsConstructor // замена конструктора
public class UsersServiceImpl { // бизнес-логика:

    private final UsersRepository usersRepository; // меняем на интерфейс del-FileImpl
    private final EmailSimpleValidator emailValidator;
    private final PasswordLengthValidator passwordValidator;

//    public UsersServiceImpl(String fileName) { // передаем репозиторий
//        // this.usersRepository = new UsersRepositoryFileImpl();
//        this.usersRepository = new UsersRepositoryFileImpl(fileName);
//        this.emailValidator = new EmailSimpleValidator();
//        this.passwordValidator = new PasswordLengthValidator();
//    }

    public UsersServiceImpl(UsersRepository usersRepository,
                            PasswordLengthValidator passwordValidator) { // передаем репозиторий
        this.usersRepository = usersRepository;
        this.emailValidator = new EmailSimpleValidator();
        this.passwordValidator = passwordValidator; // как готовый принимаю
    }

    // что бы зарегистрировать нужно сохранить пользователя:
    public void singUp(String email, String password) {

        emailValidator.validate(email);
        passwordValidator.validate(password);

        User user = User.builder()
                .email(email)
                .password(password)
                .build();
        usersRepository.save(user);
    }
}
