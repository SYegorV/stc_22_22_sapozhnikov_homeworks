package org.example;

import com.zaxxer.hikari.HikariDataSource;
import org.example.repository.impl.UsersRepositoryFileImpl;
import org.example.repository.impl.UsersRepositoryJDBCImpl;
import org.example.services.UsersServiceImpl;
import org.example.validator.PasswordLengthValidator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {

        Properties dbProperties = new Properties();
        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Main.class.getResourceAsStream("/application.properties"))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));




        System.out.println("hello");
        // сначало репозиторий:
        // UsersRepositoryFileImpl usersRepositoryFiles = new UsersRepositoryFileImpl("users.txt"); // write in file
        UsersRepositoryJDBCImpl usersRepositoryJDBC = new UsersRepositoryJDBCImpl(dataSource); // передаем подключение к базе // write in DB
        PasswordLengthValidator passwordValidator = new PasswordLengthValidator();
        // passwordValidator.setMinLength(4); // т.к. @Component - setter не требуется

        UsersServiceImpl usersService = new UsersServiceImpl(usersRepositoryJDBC, passwordValidator); // write in ** //
        usersService.singUp("08main@gmail.com", "123456main08");
    }
}