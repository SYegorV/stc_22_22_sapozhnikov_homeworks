package org.example.validator;

public class EmailSimpleValidator {
    public void validate(String email) {
        if (!email.contains("@")) { // если не содержит
            throw new IllegalArgumentException("Incorrect email format");
        }
    }
}
