package org.example.validator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component // свое_id=--->>>(value = "passwordValidator") // создать bean // но не видит--->component-scan--->видит
public class PasswordLengthValidator {
    @Value("${password.validator.length}") // отображение 4
    private int minLength; // @Value: - final - не указывается, через конструктор можно как вариант

//    public PasswordLengthValidator(@Value("${password.validator.length}") int minLength) { // for final
//        this.minLength = minLength;
//    }

    public void validate(String password) {
        if (password.length() < minLength) {
            throw new IllegalArgumentException("Incorrect password format");
        }
    }
//    public void setMinLength(int minLength) { // т.к. @Component - setter не требуется
//        this.minLength = minLength;
//    }
}
