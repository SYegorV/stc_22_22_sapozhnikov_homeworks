package org.example.repository;

import org.example.model.User;

public interface UsersRepository {
    void save(User user);
}
