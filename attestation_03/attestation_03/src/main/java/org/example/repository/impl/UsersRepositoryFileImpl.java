package org.example.repository.impl;

import org.example.model.User;
import org.example.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@Component
public class UsersRepositoryFileImpl implements UsersRepository {

    // private String fileName = "users.txt";
    @Value("${files.path}") // files.path ---> отображение users.txt - визуальное
    private String fileName; // final не требуется
//    public UsersRepositoryFileImpl(String fileName) {
//        this.fileName = fileName;
//    }

    @Override
    public void save(User user) {
        try (FileWriter fileWriter = new  FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            String userToSave = user.getEmail() + "|" + user.getPassword();
            bufferedWriter.write(userToSave);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

}
