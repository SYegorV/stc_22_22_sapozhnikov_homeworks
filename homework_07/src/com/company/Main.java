package com.company;

public class Main {
    public static int calcSumOfArrayRange(int[] arr, int from, int to) { // function
        int sum = 0;
        if (from <= to) {
            for (int i = from; i <= to; i++) {
                sum += arr[i];
            }
        } else {
            sum = -1;
            System.out.println("интервал задан неверно (значение левой границы больше, чем правой)");
        }
        return sum;
    }

    public static void isEven(int[] arr) { // procedure
        for (int i = 0; i < arr.length ; i++) {
            if (arr[i] % 2 == 0) {
                System.out.println("isEven[" + i + "] = " + arr[i]);
            } else {

            }
        }
    }

    public static void main(String[] args) {
        int[] array = {34, 10, 11, 56, 57, 93, 10};
        int sum = calcSumOfArrayRange(array, 0, 2); // function // range(диапазон)
        System.out.println("sum = " + sum);

        isEven(array); // call procedure
    }
}
