package com.company;

public class Main {
    public static void completeAllTasksOne(Task[] tasks) {
        for (Task isVal : tasks ) {
            isVal.complete();
        }
    }

    public static void main(String[] args) {
        Task even = new EvenNumbersPrintTask(1,7);
        Task odd = new OddNumbersPrintTask(11,17);
        completeAllTasksOne(new Task[] {even, odd});
    }
}