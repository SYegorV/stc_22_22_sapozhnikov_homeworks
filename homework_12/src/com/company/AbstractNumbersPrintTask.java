package com.company;

public abstract class AbstractNumbersPrintTask implements Task {
    int from;
    int to;

    public AbstractNumbersPrintTask() {
    }

    public AbstractNumbersPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }
}