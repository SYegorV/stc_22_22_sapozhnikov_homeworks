package com.company;

public interface Task {
    public abstract void complete();
}
