package com.company;

public class Figure implements Info {
    public void showPerimeterInfo() {
    }

    public void showAreaInfo() {
    }

    public int moveInfo() {
        return 0;
    }

    public static void main(String[] args) {
        Square square = new Square(4488); // квадрат
        Rectangle rectangle = new Rectangle(4488, 4488); // прямоугольник
        Circle circle = new Circle(4488); // круг
        Ellipse ellipse = new Ellipse(4488,4488); // эллипс

        // Square : квадрат :
        square.showPerimeterInfo(); // вызов метода
        square.showAreaInfo();

        // Rectangle : прямоугольник :
        rectangle.showPerimeterInfo();
        rectangle.showAreaInfo();

        // Circle : круг :
        circle.showPerimeterInfo();
        circle.showAreaInfo();

        // Ellipse : эллипс :
        ellipse.showPerimeterInfo();
        ellipse.showAreaInfo();
    }
}
