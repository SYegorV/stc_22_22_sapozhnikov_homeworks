package com.company;

public class Rectangle extends Figure {
    int height;
    int width;

    int length;

    public Rectangle(int height, int width) { // конструктор
        this.height = height;
        this.width = width;
    }

    public Rectangle(int length) { // конструктор
        this.length = length;
    }

    public void showPerimeterInfo() {
        System.out.printf("Rectangle Perimeter = %.2f [m]   ", ( (this.height + this.width) * 2.00 ) );
    }

    public void showAreaInfo() {
        System.out.println("Rectangle Area = " + ( this.height * this.width ) + " [m^2]");
    }
}
