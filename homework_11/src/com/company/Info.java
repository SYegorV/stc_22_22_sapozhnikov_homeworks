package com.company;

public interface Info {
    void showPerimeterInfo(); // периметр
    void showAreaInfo(); // площадь
    int moveInfo(); // перемещение фигуры
}
