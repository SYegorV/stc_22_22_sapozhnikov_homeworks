package com.company;

public class Square extends Rectangle {
    public Square(int length) { // конструктор
        super(length);
    }

    public void showPerimeterInfo() {
        System.out.printf("Square Perimeter = %.2f [m]   ", + ( (super.length + super.length) * 2.00 ) );
    }

    public void showAreaInfo() {
        System.out.println("Square Area = " + ( super.length * super.length ) + " [m^2]");
    }
}
