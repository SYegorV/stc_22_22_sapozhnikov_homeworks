package com.company;

public class Circle extends Ellipse {
    public Circle(int radius) { // конструктор
        super(radius);
    }

    public void showPerimeterInfo() { // длина окружности
        System.out.printf("Circle Perimeter = %.2f [m]   ", ( 2 * 3.14 * super.radius ));
    }

    public void showAreaInfo() {
        System.out.printf("Circle Area = %.2f [m^2]   \n", ( 3.14 * (super.radius * super.radius) ));
        // System.out.println("Circle Area = " + ( 3.14 * (super.radius * super.radius) ) + " [m^2]");
    }
}
