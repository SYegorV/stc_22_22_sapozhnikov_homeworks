package com.company;

public class Ellipse extends Figure {
    int smallRadius;
    int largeRadius;

    int radius;

    public Ellipse(int smallRadius, int largeRadius) { // конструктор
        this.smallRadius = smallRadius;
        this.largeRadius = largeRadius;
    }

    public Ellipse(int radius) { // конструктор
        this.radius = radius;
    }

    public void showPerimeterInfo() {
        System.out.printf("Ellipse Perimeter = %.2f [m]   ", (4 * ((3.14 * this.largeRadius * this.smallRadius + (this.largeRadius - this.smallRadius) ) / (this.largeRadius + this.smallRadius))));
    }

    public void showAreaInfo() {
        System.out.printf("Ellipse Area = %.2f [m^2]   \n", ( 3.14 * (this.largeRadius * this.smallRadius) ) );
    }
}
