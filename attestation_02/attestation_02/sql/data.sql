insert into table_user (first_name, last_name, phone, experience, age, driver_license, Category_license, rating)
values ('catherine', 'bell', '8-123-345-12-64', '2-years', 26, true, 'B', 4),
       ('alexandra', 'stone', '8-422-765-88-22', '3-years', 28, true, 'B', 5),
       ('amanda', 'green', '8-189-808-80-08', '1-years', 27, true, 'B', 4),
       ('ruby', 'kelly', '8-577-234-54-22', '4-years', 30, true, 'B', 4),
       ('anastasia', 'perry', '8-800-888-88-88', '2-years', 28, true, 'B', 5);

insert into table_car (model, color, number, Owner_ID)
values ('Kia', 'orange', '222', 'ZX4576&#'),
       ('BMW', 'blue', '848', 'FD9834<@'),
       ('Mercedes-Benz', 'yellow-red', '555', 'JK3976^$'),
       ('Tesla', 'red', '444', 'LP7843&>'),
       ('Bentley', 'blue', '888', 'RY3312>*');

insert into table_excursion (id_driver, id_car, travel_date, duration_travel)
values ('fghj34pogf', '6439erd', '2023-12-27', '2024-02-27'),
       ('vbhk76dfcv', '3423fgn', '2023-06-27', '2023-08-27'),
       ('pokmr8h8df', '1837mjk', '2023-02-02', '2023-03-02'),
       ('jalo3b7mgh', '3428dfc', '2023-04-15', '2023-06-15'),
       ('pans8d9nyu', '2328lkj', '2023-08-03', '2023-11-03');