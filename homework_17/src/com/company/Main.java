package com.company;

import com.company.Ex.List;

public class Main {

    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<>();

        integerList.add(8);
        integerList.add(10);
        integerList.add(11);
        integerList.add(13);
        integerList.add(11);
        integerList.add(15);
        integerList.add(-5);

        System.out.println("ArrayList");
        //System.out.println(integerList.size()); // 7

        for (int i = 0; i < integerList.size(); i++) {
            System.out.println(integerList.get(i));
        }
        //integerList.removeAt(4); // removeAt

        integerList.remove(11); // remove

        System.out.println("ArrayList");
        for (int i = 0; i < integerList.size(); i++) {
            System.out.println(integerList.get(i));
        }
    }
}