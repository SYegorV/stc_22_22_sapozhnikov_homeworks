package com.company.Ex;

public interface Collection<T> { // параметризация интерфейса дженерик
    /**
     * добавляет элемент в коллекцию
     * @param element добавляемый элемент
     */
    void add(T element);

    /**
     * удаляет элемент из коллекции с помощью <code>boolean equals()</code>
     * @param element удаляемый элемент
     */
    void remove(T element);

    /**
     * проверяет наличие элемента в коллекции
     * @param element искомый элемент
     * @return <code>true</code>, если элемент найден <br>
     * <code>false</code>, если элемент не найден
     */
    boolean contains(T element); // содержит  // true - содержит, false - элемент отсутствует

    /**
     * возвращает количество элементов в коллекции
     * @return число, равное количеству добавленных элементов
     */
    int size();
}
