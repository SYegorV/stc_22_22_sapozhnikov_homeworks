package com.company.Ex;

public interface List<E> extends Collection<E> { // Collection<E> буква как у List<E> - E подставляется в Collection<> = Collection<E>
    /**
     * удалить элемент по индексу
     * @param index индекс элемента
     */
    void removeAt(int index);

    /**
     * получить элемент по индексу
     * @param index индекс элемента
     * @return значение, которое лежит под индексом, либо <code>null</code>
     */
    E get(int index);
}
