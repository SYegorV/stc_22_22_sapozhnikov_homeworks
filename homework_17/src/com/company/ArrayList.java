package com.company;

import com.company.Ex.List;

public class ArrayList<P> implements List<P> { // список на основе массива
    private final static int DEFAULT_ARRAY_SIZE = 10;

    // поле массив, которое хранить все элементы
    private P[] elements;

    // поле, которое хранит число элементов
    private int count;

    public ArrayList() { // конструктор
        this.elements = (P[]) new Object[DEFAULT_ARRAY_SIZE]; // конструкция (P[])
        this.count = 0;
    }

    @Override
    public void removeAt(int index) {
        for (int i = index; i < count - 1; i++) { // сдвигаем
            elements[i] = elements[i + 1];
        }
        elements[count - 1] = null;
        count--;
    }

    @Override
    public P get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        }
        return null; // т е по индексу нету
    }

    @Override
    public void add(P element) {
        if (isFull()) { // если массив полон, создаем новый массив
            int currentLength = elements.length;
            int newLength = currentLength + currentLength / 2;
            P[] newElements = (P[]) new Object[newLength]; // конструкция (P[])

            for (int i = 0; i < count; i++) { // copy
                newElements[i] = elements[i];
            }
            // переключить ссылку на новый массив:
            this.elements = newElements;
        }
        if (count < elements.length) {
            elements[count] = element;
            count++;
        }
    }

    private boolean isFull() {
        return count == elements.length;
    }

    @Override
    public void remove(P element) {
        for (int i = 0; i < count; i++) {
            if (elements[i].equals(element)) { // если элемент найден
                removeAt(i);
                return;
            }
        }
    }

    @Override
    public boolean contains(P element) {
        // по всем элементам:
        for (int i = 0; i < count; i++) {
            // если элемент найден:
            if (elements[i].equals(element)) {
                return true;
            }
        }
        // если элемент не был найден:
        return false;
    }

    @Override
    public int size() {
        return count;
    }
}
